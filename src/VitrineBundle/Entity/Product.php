<?php

namespace VitrineBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\MappedSuperclass;
use Symfony\Component\Validator\Constraints as Assert;
use ApiPlatform\Core\Annotation\ApiResource;
use Symfony\Component\Serializer\Annotation\Groups;
use ApiPlatform\Core\Annotation\ApiSubresource;

/**
 * @ORM\Entity
 * @apiResource
 * @MappedSuperclass
 * @ORM\InheritanceType("SINGLE_TABLE")
 * @ORM\DiscriminatorColumn(name="product", type="string")
 */
abstract class Product
{
    /**
     * @var integer
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=false)
     */
    protected $name;

    /**
     * @var int
     *
     * @ORM\Column(name="price", type="integer", length=10, nullable=false)
     */
    protected $price;

    /**
     * @ORM\Column(name="description", type="text", nullable=false)
     */
    protected $description;

    /**
     * @var int
     *
     * @ORM\Column(name="quantity", type="integer", length=20, nullable=false)
     */
    protected $quantity;

    /**
     * @var string
     *
     * @ORM\Column(name="image", type="string", nullable=true)
     */
//@Assert\File(mimeTypes={ "image/jpeg","image/png" })
    protected $image;

    /**
     * @var int
     *
     * @ORM\Column(name="sellNumber", type="integer", length=20, nullable=false)
     */
    protected $sellNumber;

    /**
     * @ORM\ManyToOne(targetEntity="Category", inversedBy="products")
     * @ORM\JoinColumn(name="category_id", referencedColumnName="id")
     * @Groups({"products"})
     */
    protected $category;

    /**
     * @ORM\OneToMany(targetEntity="CommandLine", mappedBy="product")
     */
    protected $commandLines;

    public function __construct()
    {
        $this->sellNumber = 0;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @param int $price
     */
    public function setPrice($price)
    {
        $this->price = $price;
    }

    /**
     * @param string $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @param int $quantity
     */
    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;
    }

    /**
     * @param string $image
     */
    function setImage($image)
    {
        $this->image = $image;
    }

    /**
     * @param int $sellNumber
     */
    function resetSellNumber($sellNumber = 0)
    {
        $this->sellNumber = $sellNumber;
    }

    /**
     * @param int $sellNumber
     */
    function addSellNumber($sellNumber)
    {
        $this->sellNumber += $sellNumber;
    }

    /**
     * @param Category $category
     */
    public function setCategory($category)
    {
        $this->category = $category;
    }

    /**
     * @return int $id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string $name
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return int $price
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @return string $description
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @return int $quantity
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * Get image
     *
     * @return string
     */
    function getImage() {
        return $this->image;
    }

    /**
     * Get sellNumber
     *
     * @return int
     */
    function getSellNumber() {
        return $this->sellNumber;
    }

    /**
     * @return Category $category
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * @return mixed
     */
    public function getCommandLine()
    {
        return $this->commandLines;
    }

    /**
     * @param mixed $commandLine
     */
    public function setCommandLine($commandLine)
    {
        $this->commandLines = $commandLine;
    }
}

