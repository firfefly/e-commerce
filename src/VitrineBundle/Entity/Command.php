<?php

namespace VitrineBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Annotation\ApiSubresource;

/**
 * Command
 *
 * @ApiResource
 * @ORM\Table(name="command")
 * @ORM\Entity(repositoryClass="VitrineBundle\Repository\CommandRepository")
 */
class Command
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * var DateTime
     * @ORM\Column(name="date", type="datetime", options={"default": "CURRENT_TIMESTAMP"})
    */
    private $date;

    /**
     * @var bool
     * @ORM\Column(name="validate", type="boolean", nullable=true, options={"default":false})
     */
    private $validate;

    /**
     * @ORM\OneToMany(targetEntity="CommandLine", mappedBy="command")
     */
    private $commandLines;

    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="commands")
     * @ORM\JoinColumn(name="customer_id", referencedColumnName="id")
     */
    private $customer;

    /**
     * Command constructor.
     */
    public function __construct()
    {
        $this->commandLines = new ArrayCollection();
    }

    /**
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @param \DateTime $date
     */
    public function setDate($date)
    {
        $this->date = $date;
    }

    /**
     * @return bool
     */
    public function isValidate()
    {
        return $this->validate;
    }

    /**
     * @param bool $validate
     */
    public function setValidate($validate)
    {
        $this->validate = $validate;
    }

    /**
     * @return User
     */
    public function getCustomer()
    {
        return $this->customer;
    }

    /**
     * @param User $customer
     */
    public function setCustomer($customer)
    {
        $this->customer = $customer;
    }

    /**
     * @return arrayCollection
     */
    public function getCommandLines()
    {
        return $this->commandLines;
    }

    /**
     * @param array $commandLines
     */
    public function setCommandLines(array $commandLines)
    {
        $this->commandLines = $commandLines;
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }
}

