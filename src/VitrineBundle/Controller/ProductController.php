<?php

namespace VitrineBundle\Controller;


use Doctrine\ORM\EntityManager;
use Doctrine\ORM\OptimisticLockException;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use VitrineBundle\Entity\Beer;
use VitrineBundle\Form\BeerType;

class ProductController extends Controller
{
    /**
     * @Route("/products", name="products")
     */
    public function productsAction()
    {
        /** @var EntityManager $em */
        $em = $this->getDoctrine()->getManager();

        $beers = $em->getRepository(Beer::class)->findAll();

        $mostSaleBeers = $em->getRepository(Beer::class)->mostSaled();

        return $this->render('VitrineBundle:Template:products.html.twig', array(
            'beers' => $beers,
            'mostSaleBeers' => $mostSaleBeers
        ));
    }

    /**
     * @Route("/productDetails/{productId}", name="productDetails")
     *
     * @param $productId
     *
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @throws \Exception
     */
    public function productDetailsAction($productId)
    {
        $beer = $this->getDoctrine()
            ->getManager()
            ->getRepository(Beer::class)
            ->findOneBy(array('id' => $productId));

        if (empty($beer)) {
            throw new \Exception('La recherche ne correspond à aucun produit.');
        }

        return $this->render('VitrineBundle:Template:productDetails.html.twig', array(
            'beer' => $beer,
        ));
    }

    /**
     * @Route("/admin/addBeer", name="addBeer")
     *
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function addBeerAction(Request $request)
    {
        $form = $this->createForm(BeerType::class);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            /** @var Beer $beer */
            $beer = $form->getData();

            /** @var UploadedFile $file */
            $file = $beer->getImage();

            $fileName = $file->getClientOriginalName();

            $path = $this->getParameter('vitrineImagePath') . $beer->getCategory()->getName();

            $file->move($path, $fileName);

            $beer->setImage($fileName);

            $em = $this->getDoctrine()->getManager();
            $em->persist($beer);
            $em->flush();

            /** @var Session $session */
            $session = $this->get('session');

            $session->getFlashBag()->add('notice', 'L\'objet a bien été enregistré.');

            $form = $this->createForm(BeerType::class);

            return $this->render('VitrineBundle:Template:addBeer.html.twig', array(
                'form' => $form->createView()
            ));
        }

        return $this->render('VitrineBundle:Template:addBeer.html.twig', array(
            'form' => $form->createView()
        ));
    }

    /**
     * @Route("/admin/removeBeer/{beerId}", name="removeBeer")
     *
     * @throws \Exception
     */
    public function removeBeerAction(Request $request, $beerId) {
        /** @var EntityManager $em */
        $em = $this->getDoctrine()->getManager();

        /** @var Beer $beer */
        $beer = $em->getRepository(Beer::class)->find($beerId);

        $commandLines = $beer->getCommandLine();

        if (null !== $commandLines) {
            foreach ($commandLines as $commandline) {
                $command = $commandline->getCommand();

                $em->remove($commandline);
                $em->remove($command);
            }
        }

        $em->flush();

        $em->remove($beer);

        try {
            $em->flush();
        } catch (OptimisticLockException $e) {
            throw new \Exception('La bière n\'a pas pu être supprimée');
        }

        $beerImageName = $beer->getImage();

        $imageDirectory = $this->container->getParameter('vitrineImagePath');

        $fileSystem = new Filesystem();

        $fileSystem->remove($imageDirectory.'/'.$beer->getCategory()->getName().'/'.$beerImageName);

        $this->addFlash('success', 'La bière a bien été supprimée.');

        return $this->redirectToRoute('products');
    }
}