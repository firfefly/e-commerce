<?php

namespace VitrineBundle\Controller;

use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use VitrineBundle\Entity\Command;
use VitrineBundle\Entity\Product;
use VitrineBundle\Entity\User;
use Symfony\Component\Routing\Annotation\Route;

class CommandController extends Controller
{
    /**
     * @Route("/commands", name="commands")
     */
    public function viewCommandsAction()
    {
        /** @var User $user */
        $user = $this->getUser();

        $waitingCommands = $this->getDoctrine()
            ->getManager()
            ->getRepository(Command::class)
            ->findBy(array('customer' => $user, 'validate' => '0'), array('date' => 'DESC'));

        return $this->render('VitrineBundle:Template:viewCommands.html.twig', array(
            'commands' => $waitingCommands,
            'validate' => false,
        ));
    }

    /**
     * @Route("/validatedCommands", name="validatedCommands")
     */
    public function viewValidatedCommandsAction()
    {
        /** @var User $user */
        $user = $this->getUser();

        $validateCommands = $this->getDoctrine()
            ->getManager()
            ->getRepository(Command::class)
            ->findBy(array('customer' => $user, 'validate' => '1'), array('date' => 'DESC'));

        return $this->render('VitrineBundle:Template:viewCommands.html.twig', array(
            'commands' => $validateCommands,
            'validate' => true,
        ));
    }

    /**
     * @Route("/validateCommands", name="validateCommands")
     */
    public function validateCommandsAction()
    {
        $unvalidateCommands = $this->getDoctrine()
            ->getManager()
            ->getRepository(Command::class)
            ->findBy(array('validate' => '0'), array('date' => 'ASC'));

        return $this->render('VitrineBundle:Template:viewCommands.html.twig', array(
            'commands' => $unvalidateCommands,
            'validate' => false,
        ));
    }

    /**
     * @Route("/validCommand/{commandId}", name="validCommand")
     */
    public function validCommandAction($commandId)
    {
        /** @var EntityManager em */
        $em = $this->getDoctrine()->getManager();

        /** @var Command $command */
        $command = $em
            ->getRepository(Command::class)
            ->findOneBy(array('id' => $commandId));

        $command->setValidate(true);

        $em->persist($command);

        foreach($command->getCommandLines() as $commandLine) {
            /** @var Product $product */
            $product = $commandLine->getProduct();

            $product->setQuantity($product->getQuantity() - $commandLine->getQuantity());

            $product->addSellNumber($commandLine->getQuantity());

            $em->persist($product);
        }

        $em->flush();

        $unvalidateCommands = $this->getDoctrine()
            ->getManager()
            ->getRepository(Command::class)
            ->findBy(array('validate' => '0'), array('date' => 'ASC'));

        return $this->render('VitrineBundle:Template:viewCommands.html.twig', array(
            'commands' => $unvalidateCommands,
            'validate' => false,
        ));
    }
}