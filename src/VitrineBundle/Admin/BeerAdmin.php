<?php
namespace VitrineBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;

class BeerAdmin extends AbstractAdmin
{
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper->add('name', 'text');
        $formMapper->add('price', 'integer');
        $formMapper->add('description', 'text');
        $formMapper->add('quantity', 'integer');
        $formMapper->add('alcoholLevel', 'integer');
        $formMapper->add('type', 'text');
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper->add('name');
        $datagridMapper->add('price');
        $datagridMapper->add('description');
        $datagridMapper->add('quantity');
        $datagridMapper->add('alcoholLevel');
        $datagridMapper->add('type');
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper->addIdentifier('name');
        $listMapper->addIdentifier('price');
        $listMapper->addIdentifier('description');
        $listMapper->addIdentifier('quantity');
        $listMapper->addIdentifier('alcoholLevel');
        $listMapper->addIdentifier('type');
    }
}