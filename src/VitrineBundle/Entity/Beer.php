<?php

namespace VitrineBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Annotation\ApiSubresource;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Beer
 *
 * @ApiResource(
 *     collectionOperations={
 *          "get"={"method"="GET"},
 *          "post"={"method"="POST"}
 *     },
 *     itemOperations={"get"={"method"="GET"}},
 *     attributes={"filters"={"beer.search_filter"}}
 * )
 * @ORM\Table(name="beer")
 * @ORM\Entity(repositoryClass="VitrineBundle\Repository\BeerRepository")
 */
class Beer extends Product
{
    /**
     * @var int
     *
     * @ORM\Column(name="alcoholLevel", type="integer", length=5, nullable=false)
     * @Assert\Type(
     *     type="integer",
     *     message="The value {{ value }} is not a valid {{ type }}."
     * )
     */
    private $alcoholLevel;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=255, nullable=false)
     * @Assert\Type("string")
     */
    private $type;

    /**
     * @param mixed $alcoholLevel
     */
    public function setAlcoholLevel($alcoholLevel)
    {
        $this->alcoholLevel = $alcoholLevel;
    }

    /**
     * @param mixed $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @return mixed
     */
    public function getAlcoholLevel()
    {
        return $this->alcoholLevel;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }
}

