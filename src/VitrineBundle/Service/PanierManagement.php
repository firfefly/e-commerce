<?php

namespace VitrineBundle\Service;


use Doctrine\ORM\EntityManager;
use VitrineBundle\Entity\Beer;
use VitrineBundle\Entity\Panier;
use VitrineBundle\Repository\BeerRepository;

class PanierManagement
{
    /** @var $entityManager EntityManager */
    private $entityManager;

    private $tabProduct = array();

    public function __construct(EntityManager $em)
    {
        $this->entityManager = $em;
    }

    public function getEntityById(Panier $panier)
    {
        $tabId = $panier->getContenu();

        /** @var BeerRepository $beerRepo */
        $beerRepo = $this->entityManager->getRepository(Beer::class);

        foreach ($tabId as $key => $value) {

            $product = $beerRepo->findOneBy(array('id' => $key));
            $this->tabProduct[] = array($product, $value);
        }

        return $this->tabProduct;
    }
}