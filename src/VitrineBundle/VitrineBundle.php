<?php

namespace VitrineBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class VitrineBundle extends Bundle
{
    public function getParent()
    {
        return 'FOSUserBundle';
    }
}
