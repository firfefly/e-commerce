<?php

namespace VitrineBundle\Form;

use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use VitrineBundle\Entity\Beer;
use VitrineBundle\Entity\Category;

class BeerType extends ProductType
{
    public function buildForm(FormBuilderInterface $builder, array $options) {

        parent::buildForm($builder, $options);
        $builder
            ->add('alcoholLevel', NumberType::class, array('label' => 'Alcohol level', 'scale' => 2))
            ->add('type', ChoiceType::class, [
                'label' => 'Type',
                'choices' => [
                    'Pilsner' => 'Pilsner',
                    'India Pale Ale' => 'India Pale Ale',
                    'Triple' => 'Triple',
                    'Lager' => 'Lager',
                    'Porter' => 'Porter',
                    'Stout' => 'Stout',
                    'Lambic' => 'Lambic',
                    'Abbaye' => 'Abbaye'
                ],
                'expanded' => false,
            ])
            ->add('category', EntityType::class, array(
                'class' => Category::class,
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('u')
                        ->orderBy('u.name', 'ASC');
                },
                'choice_label' => 'name',
                'label' => 'Catégorie'
            ))
        ;
    }

    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults(array(
            'data_class' => Beer::class,
        ));
    }
}