<?php

namespace VitrineBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CountryType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class RegistrationType extends AbstractType {

    public function buildForm(FormBuilderInterface $builder, array $options) {
//        parent::buildForm($builder, $options);
        $builder
            ->add('firstName', TextType::class, array())
            ->add('lastName', TextType::class, array())
            ->add('address', TextType::class, array())
            ->add('country', CountryType::class, array())
            ->add('gender', ChoiceType::class, array(
                'label_attr' => array('class' => 'form-check form-check-inline'),
                'attr'       => array('class' => 'form-check-input'),
                'label' => false,
                'choice_translation_domain' => 'messages',
                'choices' => array(
                    'male' => 'male',
                    'female' => 'female',
                ),
            ));
    }

    public function getParent() {
        return 'FOS\UserBundle\Form\Type\RegistrationFormType';
    }

    public function getBlockPrefix() {
        return 'app_user_registration';
    }

    public function getName() {
        return $this->getBlockPrefix();
    }

}
