<?php

namespace VitrineBundle\Controller;

use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use VitrineBundle\Entity\Beer;
use VitrineBundle\Entity\Category;
use VitrineBundle\Form\CategoryType;

class CategoryController extends Controller
{
    /**
     * @Route("/admin/addCategory", name="addCategory")
     *
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function addCategoryAction(Request $request)
    {
        $form = $this->createForm(CategoryType::class);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            /** @var Category $category */
            $category = $form->getData();

            $em = $this->getDoctrine()->getManager();
            $em->persist($category);
            $em->flush();

            $this->addFlash('success', 'La categorie a bien été enregistré.');

            return $this->render('VitrineBundle:Template:addCategory.html.twig', array(
                'form' => $form->createView()
            ));
        }

        return $this->render('VitrineBundle:Template:addCategory.html.twig', array(
            'form' => $form->createView()
        ));
    }

    /**
     * @Route("/category/{categoryId}", name="category")
     */
    public function categoryAction($categoryId = null)
    {
        /** @var EntityManager $em */
        $em = $this->getDoctrine()->getManager();

        $categories = $em->getRepository(Category::class)->findAll();

        if ($categoryId == null) {
            $beers = $em->getRepository(Beer::class)->findAll();
        } else {
            /** @var Category $category */
            $category = $em->getRepository(Category::class)->find($categoryId);

            $beers = $category->getProducts();
        }

        return $this->render('VitrineBundle:Template:category.html.twig', array(
            'beers' => $beers,
            'categories' => $categories
        ));
    }
}