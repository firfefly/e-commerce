<?php

namespace VitrineBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ProductType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options) {

        $builder
            ->add('name', TextType::class, array('label' => 'Name'))
            ->add('price', NumberType::class, array('label' => 'Price'))
            ->add('description', TextareaType::class, array('label' => 'Description'))
            ->add('quantity', IntegerType::class, array('label' => 'Quantity'))
            ->add('image', FileType::class, array('label' => 'Image', 'data_class' => null))
        ;
    }

    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults(array(
            'inherit_data' => true,
        ));
    }
}