<?php

namespace VitrineBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use VitrineBundle\Entity\Beer;
use VitrineBundle\Entity\Panier;
use VitrineBundle\Entity\Product;
use VitrineBundle\Form\BeerType;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="index")
     */
    public function indexAction()
    {
        $session = $this->get('session');

        $session->set('panier', new Panier());

        return $this->render('VitrineBundle:Template:index.html.twig', array());
    }


}
