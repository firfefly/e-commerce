<?php

namespace VitrineBundle\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use VitrineBundle\Entity\Beer;
use VitrineBundle\Entity\Command;
use VitrineBundle\Entity\CommandLine;
use VitrineBundle\Entity\Panier;
use VitrineBundle\Service\PanierManagement;

class PanierController extends Controller
{
    /**
     * @Route("/contenuPanier", name="contenuPanier")
     */
    public function contenuPanierAction()
    {
        $session = $this->get('session');

        if (!$session->has('panier')) {
            $session->set('panier', new Panier());
        }

        /** @var PanierManagement $panierService */
        $panierService = $this->get('panierManagement');

        $contenu = $panierService->getEntityById($session->get('panier'));

        $totalPrice = 0;

        if (!empty($contenu)) {
            foreach ($contenu as $product) {
                if (null !== $product[0]) {
                    /** @var Beer $beer */
                    $beer = $product[0];

                    $quantity = $product[1];

                    $totalPrice += $beer->getPrice() * $quantity;
                }
            }
        }

        return $this->render('VitrineBundle:Template:contenuPanier.html.twig', array(
            'contenuPanier' => $contenu,
            'totalPrice' => $totalPrice,
        ));
    }

    /**
     * @Route("/ajoutArticle/{id}/{quantity}", name="ajoutArticle")
     */
    public function ajoutArticleAction(Request $request, $id, $quantity = 1)
    {
        $session = $this->get('session');

        if (!$session->has('panier')) {
            /** @var Panier $panier */
            $panier = new Panier();
        } else {
            /** @var Panier $panier */
            $panier = $session->get('panier');
        }

        if (!empty($request->request->get('quantity'))) {
            $quantity = $request->request->get('quantity');
        }

        $panier->ajoutArticle($id, $quantity);

        $session->set('panier', $panier);

        return $this->redirectToRoute('contenuPanier');
    }

    /**
     * @Route("/removeArticle/{id}", name="removeArticle")
     */
    public function removeArticleAction(Request $request, $id)
    {
        $session = $this->get('session');

        /** @var Panier $panier */
        $panier = $session->get('panier');

        $panier->supprimeArticle($id);

        $session->set('panier', $panier);

        return $this->redirectToRoute('contenuPanier');
    }

    /**
     * @Route("/validateCart/{totalPrice}", name="validateCart")
     *
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @throws \Exception
     */
    public function validateCartAction($totalPrice)
    {
        $session = $this->get('session');

        /** @var Panier $panier */
        $panier = $session->get('panier');

        if (0 !== $panier->getProductNumber()) {
            /** @var PanierManagement $panierService */
            $panierService = $this->get('panierManagement');

            $contenu = $panierService->getEntityById($session->get('panier'));

            $em = $this->getDoctrine()->getManager();

            $command = new Command();

            $command->setCustomer($this->getUser());
            $command->setDate(new \DateTime());
            $command->setValidate(false);

            $em->persist($command);

            $em->flush();

            $commandLines = array();

            foreach ($contenu as $product) {
                if (null !== $product[0]) {
                    /** @var Beer $beer */
                    $beer = $product[0];

                    $quantity = $product[1];

                    $commandLine = new CommandLine();
                    $commandLine->setPrice($beer->getPrice() * $quantity);
                    $commandLine->setProduct($beer);
                    $commandLine->setQuantity($quantity);
                    $commandLine->setCommand($command);

                    $commandLines[] = $commandLine;

                    $em->persist($commandLine);
                }
            }

            $command->setCommandLines($commandLines);

            $em->flush();

            $panier->viderPanier();

            $session->getFlashBag()->add('success', 'Votre commande a bien été validée.');
        } else {
            dump('la');
            $session->getFlashBag()->add('error', 'Vous n\'avez pas de nouvelles commandes.');

            $command = null;
        }

        return $this->render('VitrineBundle:Template:contenuCommand.html.twig', array(
            'command' => $command,
            'totalPrice' => $totalPrice,
        ));
    }
}