<?php


namespace VitrineBundle\Service;


use Doctrine\ORM\EntityManager;
use VitrineBundle\Entity\Beer;
use VitrineBundle\Entity\ListProducts;
use VitrineBundle\Entity\Product;

class TopService
{
    /** @var $entityManager EntityManager */
    private $em;

    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    public function hello()
    {
        /**
         * @var $product Product
         */
        $mostSaled = $this->em->getRepository(Beer::class)->mostSaled();


        return $mostSaled[0]->getName();
    }
}