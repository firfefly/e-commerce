<?php


namespace VitrineBundle\Controller;


use PHP2WSDL\PHPClass2WSDL;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use VitrineBundle\Service\TopService;


class SoapController extends Controller
{

    /**
     * @Route("/soap")
     */
    public function indexAction()
    {
        $soapServer = new \SoapServer('../products.wsdl');

        $topService = $this->get('TopService');

        $soapServer->setObject($topService);

        $response = new Response();
        $response->headers->set('Content-Type', 'text/xml; charset=ISO-8859-1');

        ob_start();
        $soapServer->handle();
        $response->setContent(ob_get_clean());

        return $response;
    }

}